<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see \craft\config\GeneralConfig
 */

use craft\helpers\App;

$isDev = App::env('ENVIRONMENT') === 'dev';
$isProd = App::env('ENVIRONMENT') === 'production';

return [
    // Environment variable aliases for control panel
    'aliases' => [
        '@rootUrl' => App::env('PRIMARY_SITE_URL'),
        '@rootPath' => App::env('PRIMARY_SITE_PATH'),
    ],

    // Disable administrative changes outside the dev environment
    'allowAdminChanges' => $isDev,

    // Disable CMS and plugin updates outside dev environment
    'allowUpdates' => $isDev,

    // Cache data and RSS feeds indefinitely, and template caches for 1 year
    'cacheDuration' => 0,

    // The URI segment that tells Craft to load the control panel
    'cpTrigger' => App::env('CP_TRIGGER') ?: 'admin',

    // Expire tokens after 7 days (duration in seconds)
    'defaultTokenDuration' => 604800,

    // Default Week Start Day (0 = Sunday, 1 = Monday...)
    'defaultWeekStartDay' => 0,

    // Enable dev mode in dev environment
    'devMode' => $isDev,

    // Disallow robots outside of production environment
    'disallowRobots' => !$isProd,

    // Disable GraphQL
    'enableGql' => false,

    // Omit "index.php" from generated URLs
    'omitScriptNameInUrls' => true,

    // Always return a successful response in the forgot password flow
    'preventUserEnumeration' => true,

    // Disable public template routing outside dev environment
    'privateTemplateTrigger' => $isDev ? '_' : '',

    // The secure key Craft will use for hashing and encrypting data
    'securityKey' => App::env('SECURITY_KEY'),

    // Disable X-Powered-By header
    'sendPoweredByHeader' => false,

    // Expire user verification codes after 3 days (duration in seconds)
    'verificationCodeDuration' => 259200,
];
