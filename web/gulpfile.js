/* ----------------------------------------------------------------------- *
 * Gulp Plugins *
 * ----------------------------------------------------------------------- */

const autoprefixer = require('gulp-autoprefixer');
const babelify     = require('babelify');
const browserify   = require('browserify');
const buffer       = require('vinyl-buffer');
const cache        = require('gulp-cache');
const eslint       = require('gulp-eslint');
const gulp         = require('gulp');
const imagemin     = require('gulp-imagemin');
const rename       = require('gulp-rename');
const sass         = require('gulp-sass');
const source       = require('vinyl-source-stream');
const sourcemaps   = require('gulp-sourcemaps');
const uglify       = require('gulp-uglify');

/* ----------------------------------------------------------------------- *
 * Compile Sass *
 * ----------------------------------------------------------------------- */

{
    const globs = [
        './styles/main.scss',
    ];

    const options = {
        autoprefixer: {
            cascade: false,
        },
        sass: {
            outputStyle: 'compressed',
        },
    };

    // Use Dart Sass
    sass.compiler = require('sass');

    /* ------------ *
     * Task: styles *
     * ------------ *
     * 1. Initialize source map
     * 2. Compile SASS
     * 3. Prefix styles for browser compatibility
     * 4. Create source map
     * 5. Save CSS file
     */

    gulp.task('sass', function() {
        return gulp.src(globs)
            .pipe(sourcemaps.init())                             // 1
            .pipe(sass(options.sass).on('error', sass.logError)) // 2
            .pipe(autoprefixer(options.autoprefixer))            // 3
            .pipe(sourcemaps.write('.'))                         // 4
            .pipe(gulp.dest('./styles'));                        // 5
    });
}

/* ----------------------------------------------------------------------- *
 * Lint JavaScript *
 * ----------------------------------------------------------------------- */

{
    const globs = [
        './scripts/app.js',
        './scripts/modules/**/*.js',
        '!./scripts/libs/**/*.js',
    ];

    const options = {
        eslint: {
            configFile: './.eslintrc',
        },
    };

    /* ------------- *
     * Task: js:lint *
     * ------------- *
     * 1. Lint JavaScript
     * 2. Output linting results
     * 3. Fail on errors
     */

    gulp.task('js:lint', function() {
        return gulp.src(globs)
            .pipe(eslint(options.eslint))   // 1
            .pipe(eslint.format())          // 2
            .pipe(eslint.failAfterError()); // 3
    });
}

/* ----------------------------------------------------------------------- *
 * Compile JavaScript *
 * ----------------------------------------------------------------------- */

{
    const babel_polyfill = './scripts/polyfills/babel-polyfill.min.js';
    const app_js = './scripts/app.js';

    const options = {
        babelify: {
            presets: ['@babel/preset-env'],
        },
        browserify: {
            debug: true,
            entries: [
                babel_polyfill,
                app_js,
            ],
        },
        sourcemaps: {
            loadMaps: true,
        },
    };

    /* -------- *
     * Task: js *
     * -------- *
     * 1. Convert ES6 to ES5
     * 2. Bundle the JS modules
     * 3. Convert Browserify stream for Gulp
     * 4. Rename file to main.js
     * 5. Convert stream to use buffers
     * 6. Initialize source map
     * 7. Minify JS file
     * 8. Create source map
     * 9. Save JS file
     */

    gulp.task('js', function() {
        return browserify(options.browserify)
            .transform(babelify, options.babelify)     // 1
            .bundle()                                  // 2
            .pipe(source(app_js))                      // 3
            .pipe(rename('main.js'))                   // 4
            .pipe(buffer())                            // 5
            .pipe(sourcemaps.init(options.sourcemaps)) // 6
            .pipe(uglify())                            // 7
            .pipe(sourcemaps.write('.'))               // 8
            .pipe(gulp.dest('./scripts'));             // 9
    });
}

/* ----------------------------------------------------------------------- *
 * Image Compression *
 * ----------------------------------------------------------------------- */

{
    const globs = [
        './images/src/**/*.gif',
        './images/src/**/*.jpg',
        './images/src/**/*.png',
        './images/src/**/*.svg',
    ];

    const options = {
        imagemin: [
            imagemin.gifsicle({
                interlaced: true,
                optimizationLevel: 2,
            }),
            imagemin.jpegtran({
                progressive: true,
            }),
            imagemin.optipng({
                optimizationLevel: 5,
            }),
        ],
    };

    /* ------------ *
     * Task: images *
     * ------------ *
     * 1. Compress files
     * 2. Save images to root images folder
     */

    gulp.task('images', function() {
        return gulp.src(globs)
            .pipe(cache(imagemin(options.imagemin))) // 1
            .pipe(gulp.dest('./images'));            // 2
    });
}

/* ----------------------------------------------------------------------- *
 * File Watchers *
 * ----------------------------------------------------------------------- */

{
    const globs = {
        images: [
            './images/src/**/*.gif',
            './images/src/**/*.jpg',
            './images/src/**/*.png',
            './images/src/**/*.svg',
        ],
        js: [
            './scripts/app.js',
            './scripts/modules/**/*.js',
        ],
        sass: [
            './styles/**/*.scss',
        ],
    };

    /* ----------- *
     * Task: watch *
     * ----------- *
     * 1. Images
     * 2. JavaScript
     * 3. Sass
     */

    gulp.task('watch', function() {
        // 1. Images
        gulp.watch(
            globs.images,
            gulp.task('images'),
        );

        // 2. JavaScript
        gulp.watch(
            globs.js,
            gulp.series(
                gulp.task('js:lint'),
                gulp.task('js'),
            ),
        );

        // 3. Sass
        gulp.watch(
            globs.sass,
            gulp.task('sass'),
        );
    });
}

/* ----------------------------------------------------------------------- *
 * Task: default *
 * ----------------------------------------------------------------------- */

gulp.task(
    'default',
    gulp.parallel(
        gulp.task('sass'),
        gulp.task('images'),
        gulp.task('watch'),
        gulp.series(
            gulp.task('js:lint'),
            gulp.task('js'),
        ),
    ),
);
