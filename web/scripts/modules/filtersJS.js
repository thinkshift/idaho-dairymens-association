const filtersJS = {
    init() {
        const filters = document.querySelectorAll('.filter');
        const itemsAvailable = document.querySelectorAll('.project8 ');

        filters.forEach((filter) => {
            filter.addEventListener('click', () => {
                const filtersArray = Array.from(filters);
                filtersArray.forEach((filterItem) => {
                    filterItem.classList.remove('selected');
                });
                const selectedFilter = filter.getAttribute('data-filter');
                filter.setAttribute('class', 'selected category filter filterNav__listItem');
                let itemsToHide = document.querySelectorAll(`.projects .project:not([data-filter='${selectedFilter}'])`);
                let itemsToShow = document.querySelectorAll(`.projects [data-filter='${selectedFilter}']`);

                if (selectedFilter === 'All') {
                    itemsToHide = [];
                    itemsToShow = document.querySelectorAll('.projects [data-filter]');
                }

                itemsToHide.forEach((el) => {
                    el.parentElement.classList.add('parentHide');
                    el.classList.add('hide');
                    el.classList.remove('show');
                });

                itemsToShow.forEach((el) => {
                    el.parentElement.classList.remove('parentHide');
                    el.classList.remove('hide');
                    el.classList.add('show');
                });
            });
        });

        const filters2 = document.querySelectorAll('.filter2');
        filters2.forEach((filter) => {
            filter.addEventListener('click', () => {
                const alreadyTop = Array.from(Array.from(document.getElementsByClassName('top')));
                const alreadyActive = Array.from(document.getElementsByClassName('active'));

                alreadyActive.forEach((activeItem) => {
                    activeItem.classList.remove('active');
                });
                alreadyTop.forEach((topItem) => {
                    topItem.classList.remove('top');
                });

                const selectedFilter = filter.getAttribute('data-filter2');
                filter.classList.add('active');
                filter.parentElement.classList.add('active');
                const topItems = Array.from(document.querySelectorAll(`.projects2 [data-filter2='${selectedFilter}']`));

                itemsAvailable.forEach((item) => {
                    item.style.display = 'none';
                });

                topItems.forEach((item) => {
                    item.classList.add('top');
                    item.style.display = 'block';
                });
            });
        });
    },
};

export default filtersJS;
