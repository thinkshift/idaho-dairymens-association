/* -------------------------------------------------------------------------- *
 * Main Nav *
 * -------------------------------------------------------------------------- *
 */

const settings = {
    selector: 'todo',
};

/* ------------- *
 * Module Export *
 * ------------- */

const mainNavJS = {
    selector: settings.selector,
    init() {
        const mobileButton = document.getElementsByClassName('mainNav__mobileButton')[0];
        const topLink = Array.from(document.getElementsByClassName('mainNav__toplinkHover'));
        const desktopLogo = document.getElementsByClassName('mainNav__logo-container')[0];
        const mainNavContainer = document.getElementsByClassName('mainNav__container')[0];
        const mainNav = document.getElementsByClassName('mainNav')[0];
        const mainNavContent = Array.from(document.getElementsByClassName('mainNav__content'))[0];
        const subLink = Array.from(document.getElementsByClassName('mainNav__sublink-list'));

        mobileButton.addEventListener('click', () => {
            topLink.forEach((link) => {
                link.classList.toggle('active');
            });

            subLink.forEach((link) => {
                link.classList.toggle('active');
            });

            desktopLogo.classList.toggle('active');
            mainNavContainer.classList.toggle('active');
            mainNavContent.classList.toggle('active');
            mainNav.classList.toggle('active');
        });
    },
};

export default mainNavJS;
