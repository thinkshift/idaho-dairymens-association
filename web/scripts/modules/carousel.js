/* -------------------------------------------------------------------------- *
 * Carousel *
 * -------------------------------------------------------------------------- *
 * Accessible carousel using the slick carousel plugin.
 * https://kenwheeler.github.io/slick/
 */

/* global $ */

const settings = {
    selector: '.js-carousel',
    default: {
        dots: true,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
    },
    homepage: {
        dots: true,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
    },
    smallCarousel: {
        dots: true,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
    },
    insightsFeaturedContent: {
        dots: true,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
    },
    feature: {
        adaptiveHeight: true,
        dots: true,
        infinite: false,
    },
    gallery: {
        infinite: false,
        responsive: [
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                },
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                },
            },
            {
                breakpoint: 40,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                },
            },
        ],
        slidesToShow: 5,
        slidesToScroll: 5,
    },
};

/* ------------- *
 * Module Export *
 * ------------- */

const Carousel = {
    selector: settings.selector,

    init(selector = settings.selector) {
        const carousels = document.querySelectorAll(selector);
        const carousels_arr = Array.from(carousels);

        carousels_arr.forEach((el) => {
            const type = el.getAttribute('data-type');

            $(el).slick(settings[type]);
        });
    },
};

export default Carousel;
