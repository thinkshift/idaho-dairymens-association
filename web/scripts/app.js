/* -------------------------------------------------------------------------- *
 * JavaScript Modules *
 * -------------------------------------------------------------------------- */

import Consent from './modules/consent';
import Notification from './modules/notification';
import StickyNav from './modules/stickyNav';
import Toggle from './modules/toggle';
import LazyLoad from './modules/lazyLoad';
import mainNavJS from './modules/mainNavJS';
import filtersJS from './modules/filtersJS';
import Carousel from './modules/carousel';

/* -------------------------------------------------------------------------- *
 * Module Initialization *
 * -------------------------------------------------------------------------- */

/* ----------------- *
 * Load on All Pages *
 * ----------------- */

StickyNav.init();

/* ------------------ *
 * Load When Required *
 * ------------------ */

if (document.querySelector(Consent.selector) !== null) {
    Consent.init();
}

if (document.querySelector(Notification.selector) !== null) {
    Notification.init();
}

if (document.querySelector(Toggle.selector) !== null) {
    Toggle.init();
}

if (document.querySelector(LazyLoad.selector) !== null) {
    LazyLoad.init();
}

if (document.querySelector(Carousel.selector) !== null) {
    Carousel.init();
}

mainNavJS.init();
filtersJS.init();
